<form class="form-horizontal">
                    <div class="panel-body">
                                    <p>Explanation of what this form is for goes here.</p>
                                </div>
                                <div class="panel-body">
                                  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Division</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Enter Division</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Asset Code</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Enter the asset code</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Company</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" readonly />
                                            </div>                                            
                                            <span class="help-block">Company. This is automatically filled.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Department</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" readonly/>
                                            </div>                                            
                                            <span class="help-block">Department. This is automatically filled.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Dept. Code</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" readonly/>
                                            </div>                                            
                                            <span class="help-block">Department Code. This is automatically filled.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Asset Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <textarea class="form-control" rows="5" readonly></textarea>
                                            <span class="help-block">Asset Description. This is automatically filled.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Acquisition Date</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" readonly/>
                                            </div>                                            
                                            <span class="help-block">Acquisition Date. This is automatically filled.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Cost</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" readonly/>
                                            </div>                                            
                                            <span class="help-block">Cost. This is automatically filled.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Book Value</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" readonly/>
                                            </div>                                            
                                            <span class="help-block">Book Value. This is automatically filled.</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                <h1>Transfer Details</h1>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Transfer From:</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Company</span>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Department</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Transfer To:</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Company</span>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Department</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <button class="btn btn-primary pull-right">Submit <span class="fa fa-floppy-o fa-right"></span></button>
                                            </div>
                                        </div>
                                    </div>
                            </form> 