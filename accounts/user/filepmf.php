<?php include_once('header.php');?>

<!-- MAIN CONTENT -->                
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Dashboard</a></li>                    
</ul>
<!-- END BREADCRUMB -->                       
                
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
                  
                    
<div class="row">
    <div class="col-md-12">
        
                                                            
        <div class="panel panel-default tabs">                            
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Internal</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Temporary</a></li>
                <li><a href="#tab-third" role="tab" data-toggle="tab">External</a></li>
                <li><a href="#tab-fourth" role="tab" data-toggle="tab">Retirement</a></li>
            </ul>
            <div class="panel-body tab-content">
                <!-- Internal Tab -->
                <div class="tab-pane active" id="tab-first">
                    <?php include_once('internal.php');?>            
                </div>
                <!-- End of Internal Tab -->
                <!-- Temporary Tab -->
                <div class="tab-pane" id="tab-second">
                    <?php include_once('temporary.php');?>
                </div> 
                <!-- End of Temporary Tab -->
                <!-- External Tab -->                                       
                <div class="tab-pane" id="tab-third">
                    <?php include_once('external.php');?>
                </div>
                <!-- End of External Tab -->
                <!-- Retirement Tab -->  
                <div class="tab-pane" id="tab-fourth">
                    <?php include_once('retirement.php');?>
                </div>
                <!-- End of Retirement Tab -->
            </div>
            
        </div>                                
                            
                         
    </div>        
</div>
                    
                    <!-- START DASHBOARD CHART -->
                    <div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
                    <div class="block-full-width">
                                                                       
                    </div>                    
                    <!-- END DASHBOARD CHART -->
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->


        






<?php include_once('footer.php');?>